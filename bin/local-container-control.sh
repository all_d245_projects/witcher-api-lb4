#!/bin/bash

# Bash script to manage the startup and destruction of local containers

# docker compose command
dockerComposeCommand=$1

case $dockerComposeCommand in
  start)
    if [ $daemonStart ]; then
      docker-compose \
        -f ./docker-compose.local.yml \
        up -d \
        --build
    else
      docker-compose \
        -f ./docker-compose.local.yml \
        up -d \
        --build
    fi
    ;;

  stop)
    docker-compose \
      -f ./docker-compose.local.yml \
      down --remove-orphans
    ;;

  restart)
  docker-compose \
    -f ./docker-compose.local.yml \
    restart
  ;;

  *)
    printf "unsupported command\n"
    ;;
esac
