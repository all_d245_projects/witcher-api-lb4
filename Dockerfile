# =========================================================
# Node.js Application Dockerfile
# =========================================================
# Additional linux packages via apt-get
#
# unzip
# tar
# git
# mlocate
# iputils-ping
# curl
# nano
# =========================================================
# Additional packages via npm
#
# - pm2
# - pm2-logrotate [via pm2 install]
# =========================================================

# Check out https://hub.docker.com/_/node to select a new base image
FROM node:12-slim

LABEL MAINTAINER="Aldrich Saltson <asaltson@gmail.com>"

USER root

### install linux packages
RUN apt-get update && apt-get install -y \
  unzip \
  tar \
  git \
  mlocate \
  iputils-ping \
  curl \
  nano

### initialize mlocatedb
RUN updatedb

### install pm2
RUN npm install pm2 -g

### configure pm2-logrotate
ARG PM2_TIMEZONE
RUN pm2 set pm2-logrotate:retain 10 && \
  pm2 set pm2-logrotate:TZ ${PM2_TIMEZONE} && \
  pm2 set pm2-logrotate:compress true

# Set to a non-root built-in user `node`
USER node

# Create app directory (with user `node`)
RUN mkdir -p /home/node/app

WORKDIR /home/node/app

### copy built archive into image
ARG APP_NAME_SLUG
ARG APP_VERSION
COPY ${APP_NAME_SLUG}-${APP_VERSION}.tgz ./

### decompress archive
RUN tar -zxvf ${APP_NAME_SLUG}-${APP_VERSION}.tgz --no-same-owner

### remove archive
RUN rm ${APP_NAME_SLUG}-${APP_VERSION}.tgz

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=3000

EXPOSE ${PORT}

HEALTHCHECK --interval=5m --timeout=10s --start-period=10s \
  CMD curl -f http://localhost:3000/ping || exit 1

CMD [ "pm2-runtime", "ecosystem.config.js" ]
