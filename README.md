# witcher-api-lb4

This application is generated using [LoopBack 4 CLI](https://loopback.io/doc/en/lb4/Command-line-interface.html) with the
[initial project layout](https://loopback.io/doc/en/lb4/Loopback-application-layout.html).

Documentation can be found [here](https://all_d245_projects.gitlab.io/witcher-api-lb4/index.html)

## Install dependencies

By default, dependencies were installed when this application was generated.
Whenever dependencies in `package.json` are changed, run the following command:

```sh
npm install
```

To only install resolved dependencies in `package-lock.json`:

```sh
npm ci
```

## Run the application

```sh
npm start
```

with PM2
```sh
npm run start:pm2
```

You can also run `node .` to skip the build step.

Open http://127.0.0.1:3000 in your browser.

## Rebuild the project

To incrementally build the project:

```sh
npm run build
```

To force a full build by cleaning up cached artifacts:

```sh
npm run rebuild
```

## Fix code style and formatting issues

```sh
npm run lint
```

To automatically fix such issues:

```sh
npm run lint:fix
```

## Other useful commands

- `npm run migrate`: Migrate database schemas for models
- `npm run openapi-spec`: Generate OpenAPI spec into a file
- `npm run docker:build`: Build a Docker image for this application
- `npm run docker:run`: Run this application inside a Docker container

## Tests

This project uses MongoDB as it's datasource.
It's required to have a MongoDB server running locally.
If one is not available, you can start a MongoDB instance via docker by ensuring
Docker and Docker Compose are installed.

First prepare an environment file for the mongo db test database by creating a `.env`
file with the contents below:
`src/__tests__/fixtures/datasource/.env`
```
MONGODB_URL=mongodb://test-user:test-password@127.0.0.1:5000/witcher-api-test?authSource=witcher-api-test
```

Afterwards run the command below from the project root:
```sh
bin/local-container-control.sh start
```

```sh
npm test
```

**Note:** If you are unable to start the mongoDB container as a result of port bind clash,
change the host machine port bind in the file `docker-compose-local.yml`, and change the port
of the `MONGODB_URL` connection string in the `.env`.

### Test Coverage
Test coverage report can be found [here](https://all_d245_projects.gitlab.io/witcher-api-lb4/coverage/index.html)

## Open API Specification

Open API specification can be found [here](https://all_d245_projects.gitlab.io/witcher-api-lb4/openapi-spec/witcher-api-docs.html)
## What's next

Please check out [LoopBack 4 documentation](https://loopback.io/doc/en/lb4/) to
understand how you can continue to add features to this application.

[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
