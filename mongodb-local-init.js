print('Start #################################################################');

db = db.getSiblingDB('witcher-api-local');
db.createUser(
  {
    user: 'local-user',
    pwd: 'local-password',
    roles: [{ role: 'readWrite', db: 'witcher-api-local' }],
  },
);
db.createCollection('tmp');

db = db.getSiblingDB('witcher-api-test');
db.createUser(
  {
    user: 'test-user',
    pwd: 'test-password',
    roles: [{ role: 'readWrite', db: 'witcher-api-test' }],
  },
);
db.createCollection('tmp');

print('END #################################################################');
