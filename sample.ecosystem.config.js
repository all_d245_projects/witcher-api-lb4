module.exports = {
  apps: [
    {
      name: 'Witcher-API-LoopBack',
      script: 'dist/index.js',
      instances: 1,
      watch: false,
      env: {
        NODE_ENV: 'development',
        MONGODB_URL: '',
      },
    },
  ],
};
