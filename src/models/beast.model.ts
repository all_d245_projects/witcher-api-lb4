import {belongsTo, model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity';
import {BeastClass, BeastClassWithRelations} from './beast-class.model';

@model({
  settings: {
    mongodb: {collection: 'Beasts'},
    jsonSchema: {
      required: ['beastClassId'],
    },
  },
})
export class Beast extends BaseEntity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 1,
    },
  })
  name: string;

  @belongsTo(
    () => BeastClass,
    {},
    {
      required: true,
    },
  )
  beastClassId: string;

  constructor(data?: Partial<Beast>) {
    super(data);
  }
}

export interface BeastRelations {
  // describe navigational properties here
  beastClassId: BeastClassWithRelations;
}

export type BeastWithRelations = Beast & BeastRelations;
