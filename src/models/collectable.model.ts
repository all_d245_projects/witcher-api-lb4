import {model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity';

@model({
  settings: {
    mongodb: {collection: 'Collectables'},
  },
})
export class Collectable extends BaseEntity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  constructor(data?: Partial<Collectable>) {
    super(data);
  }
}

export interface CollectableRelations {
  // describe navigational properties here
}

export type CollectableWithRelations = Collectable & CollectableRelations;
