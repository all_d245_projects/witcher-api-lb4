import {model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity';

@model({
  settings: {
    mongodb: {collection: 'BeastClass'},
    strictObjectIDCoercion: true,
  },
})
export class BeastClass extends BaseEntity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  constructor(data?: Partial<BeastClass>) {
    super(data);
  }
}

export interface BeastClassRelations {
  // describe navigational properties here
}

export type BeastClassWithRelations = BeastClass & BeastClassRelations;
