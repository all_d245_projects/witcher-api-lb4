import {Entity, property} from '@loopback/repository';
export class BaseEntity extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
  })
  id: string;

  @property({
    type: 'date',
    required: true,
    default: new Date(),
  })
  createdAt: Date;

  @property({
    type: 'date',
  })
  updatedAt?: Date;
}
