import {model, property} from '@loopback/repository';
import {BaseEntity} from './base-entity';

@model({
  settings: {
    mongodb: {collection: 'Locations'},
  },
})
export class Location extends BaseEntity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  constructor(data?: Partial<Location>) {
    super(data);
  }
}

export interface LocationRelations {
  // describe navigational properties here
}

export type LocationWithRelations = Location & LocationRelations;
