import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {Beast} from '../../models';
import {BeastRepository} from '../../repositories';

export class BeastsController {
  constructor(
    @repository(BeastRepository)
    public beastRepository: BeastRepository,
  ) {}

  @get('/beasts/count')
  @response(200, {
    description: 'Beast model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Beast) where?: Where<Beast>): Promise<Count> {
    return this.beastRepository.count(where);
  }

  @get('/beasts')
  @response(200, {
    description: 'Array of Beast model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Beast, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Beast) filter?: Filter<Beast>): Promise<Beast[]> {
    return this.beastRepository.find(filter);
  }

  @get('/beasts/{id}')
  @response(200, {
    description: 'Beast model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Beast, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Beast, {exclude: 'where'})
    filter?: FilterExcludingWhere<Beast>,
  ): Promise<Beast> {
    return this.beastRepository.findById(id, filter);
  }

  @post('/beasts')
  @response(200, {
    description: 'Beast model instance',
    content: {'application/json': {schema: getModelSchemaRef(Beast)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Beast, {
            title: 'NewBeast',
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    beast: Omit<Beast, 'id'>,
  ): Promise<Beast> {
    return this.beastRepository.create(beast);
  }

  @patch('/beasts/{id}')
  @response(204, {
    description: 'Beast PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Beast, {
            partial: true,
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    beast: Beast,
  ): Promise<void> {
    await this.beastRepository.updateById(id, beast);
  }

  @del('/beasts/{id}')
  @response(204, {
    description: 'Beast DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.beastRepository.deleteById(id);
  }
}
