import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {BeastClass} from '../../models';
import {BeastClassRepository} from '../../repositories';

export class BeastClassController {
  constructor(
    @repository(BeastClassRepository)
    public beastClassRepository: BeastClassRepository,
  ) {}

  @post('/beast-class')
  @response(200, {
    description: 'BeastClass model instance',
    content: {'application/json': {schema: getModelSchemaRef(BeastClass)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BeastClass, {
            title: 'NewBeastClass',
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    beastClass: Omit<BeastClass, 'id'>,
  ): Promise<BeastClass> {
    return this.beastClassRepository.create(beastClass);
  }

  @get('/beast-class/count')
  @response(200, {
    description: 'BeastClass model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(BeastClass) where?: Where<BeastClass>,
  ): Promise<Count> {
    return this.beastClassRepository.count(where);
  }

  @get('/beast-class')
  @response(200, {
    description: 'Array of BeastClass model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(BeastClass, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(BeastClass) filter?: Filter<BeastClass>,
  ): Promise<BeastClass[]> {
    return this.beastClassRepository.find(filter);
  }

  @get('/beast-class/{id}')
  @response(200, {
    description: 'BeastClass model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(BeastClass, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(BeastClass, {exclude: 'where'})
    filter?: FilterExcludingWhere<BeastClass>,
  ): Promise<BeastClass> {
    return this.beastClassRepository.findById(id, filter);
  }

  @patch('/beast-class/{id}')
  @response(204, {
    description: 'BeastClass PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BeastClass, {
            partial: true,
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    beastClass: BeastClass,
  ): Promise<void> {
    await this.beastClassRepository.updateById(id, beastClass);
  }

  @del('/beast-class/{id}')
  @response(204, {
    description: 'BeastClass DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.beastClassRepository.deleteById(id);
  }
}
