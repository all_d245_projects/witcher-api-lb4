import {repository} from '@loopback/repository';
import {get, getModelSchemaRef, param} from '@loopback/rest';
import {Beast, BeastClass} from '../../models';
import {BeastRepository} from '../../repositories';

export class BeastBeastClassController {
  constructor(
    @repository(BeastRepository)
    public beastRepository: BeastRepository,
  ) {}

  @get('/beasts/{id}/beast-class', {
    responses: {
      '200': {
        description: 'BeastClass belonging to Beast',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(BeastClass)},
          },
        },
      },
    },
  })
  async getBeastClass(
    @param.path.string('id') id: typeof Beast.prototype.id,
  ): Promise<BeastClass> {
    return this.beastRepository.beastClass(id);
  }
}
