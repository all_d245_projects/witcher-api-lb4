import {MixinTarget} from '@loopback/core';
import {Model, property} from '@loopback/repository';

/**
 * A mixin factory to add timestamp properties
 *
 * @param baseClass - Base Class
 * @typeParam T - Model class
 */
export function TimeStampMixin<T extends MixinTarget<Model>>(baseClass: T) {
  class MixedModel extends baseClass {
    @property({
      type: 'date',
      required: true,
      default: new Date(),
    })
    createdAt: Date;

    @property({
      type: 'date',
    })
    updatedAt?: Date;
  }
  return MixedModel;
}
