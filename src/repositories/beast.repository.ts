import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Beast, BeastRelations} from '../models';
import {BeastClass} from '../models/beast-class.model';
import {BeastClassRepository} from './beast-class.repository';

export class BeastRepository extends DefaultCrudRepository<
  Beast,
  typeof Beast.prototype.id,
  BeastRelations
> {
  public readonly beastClass: BelongsToAccessor<
    BeastClass,
    typeof Beast.prototype.id
  >;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('BeastClassRepository')
    protected beastClassRepositoryGetter: Getter<BeastClassRepository>,
  ) {
    super(Beast, dataSource);
    this.beastClass = this.createBelongsToAccessorFor(
      'beastClass',
      beastClassRepositoryGetter,
    );
    this.registerInclusionResolver(
      'beastClass',
      this.beastClass.inclusionResolver,
    );
  }
}
