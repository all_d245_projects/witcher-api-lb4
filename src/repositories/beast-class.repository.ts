import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {BeastClass, BeastClassRelations} from '../models/beast-class.model';

export class BeastClassRepository extends DefaultCrudRepository<
  BeastClass,
  typeof BeastClass.prototype.id,
  BeastClassRelations
> {
  constructor(@inject('datasources.db') dataSource: DbDataSource) {
    super(BeastClass, dataSource);
  }
}
