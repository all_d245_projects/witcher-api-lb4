import {
  Client,
  createRestAppClient,
  givenHttpServerConfig,
} from '@loopback/testlab';
import {WitcherApiLb4Application} from '../../..';
import {testdb} from '../../fixtures/datasource';

export async function setupApplication(): Promise<AppWithClient> {
  const restConfig = givenHttpServerConfig({
    // Customize the server configuration here.
    // Empty values (undefined, '') will be ignored by the helper.
    //
    // host: process.env.HOST,
    // port: +process.env.PORT,
  });

  const app = new WitcherApiLb4Application({
    rest: restConfig,
  });

  await app.boot();
  app.dataSource(testdb);
  await app.start();

  const client = createRestAppClient(app);

  return {app, client};
}

export interface AppWithClient {
  app: WitcherApiLb4Application;
  client: Client;
}
