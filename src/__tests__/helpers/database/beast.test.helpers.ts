import {Beast, BeastClass} from '../../../models';
import {BeastClassRepository, BeastRepository} from '../../../repositories';
import {testdb} from '../../fixtures/datasource';

const ObjectId = require('mongodb').ObjectId;

export default class BeastTestHelper {
  public beastClassRepository: BeastClassRepository;
  public beastRepository: BeastRepository;

  constructor() {
    this.beastClassRepository = new BeastClassRepository(testdb);
    this.beastRepository = new BeastRepository(
      testdb,
      async () => this.beastClassRepository,
    );
  }

  public givenBeastClassData(data?: Partial<BeastClass>) {
    return Object.assign(
      {
        name: 'a-beast-class-name',
        description: 'a-beast-description',
      },
      data,
    );
  }

  async givenBeastClass(data?: Partial<BeastClass>) {
    return this.beastClassRepository.create(this.givenBeastClassData(data));
  }

  async givenBeastData(data?: Partial<Beast>) {
    const beastClass = await this.givenBeastClass();

    return Object.assign(
      {
        name: 'a-beast-name',
        beastClassId: beastClass.id,
      },
      data,
    );
  }

  async givenBeast(data?: Partial<Beast>) {
    const beastData = await this.givenBeastData(data);

    return this.beastRepository.create(beastData);
  }
}
