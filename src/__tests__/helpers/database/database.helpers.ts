import BeastTestHelper from './beast.test.helpers';

export async function givenEmptyDatabase() {
  const beastHelper = new BeastTestHelper();

  await beastHelper.beastClassRepository.deleteAll();
  await beastHelper.beastRepository.deleteAll();
}
