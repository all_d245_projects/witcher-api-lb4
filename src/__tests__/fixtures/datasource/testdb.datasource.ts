import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import dotenv from 'dotenv';
import path from 'path';

const result = dotenv.config({
  path: path.join(__dirname, '.env'),
});

if (result.error) {
  throw result.error;
}

const config = {
  name: 'db',
  connector: 'mongodb',
  url: process.env.MONGODB_URL ?? '',
  useNewUrlParser: true,
  unifiedTopology: true,
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
class TestdbDataSource extends juggler.DataSource implements LifeCycleObserver {
  static dataSourceName = 'db';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.db', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}

export const testdb: juggler.DataSource = new TestdbDataSource();
