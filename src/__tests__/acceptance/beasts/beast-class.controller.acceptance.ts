import {Client, expect} from '@loopback/testlab';
import {WitcherApiLb4Application} from '../../..';
import {BeastClass} from '../../../models';
import {setupApplication} from '../../helpers/app/app-helper';
import BeastTestHelper from '../../helpers/database/beast.test.helpers';
import {givenEmptyDatabase} from '../../helpers/database/database.helpers';

describe('BeastClassController (integration)', () => {
  let app: WitcherApiLb4Application;
  let client: Client;
  let beastTestHelper: BeastTestHelper;

  before('setupApplication', async () => {
    ({app, client} = await setupApplication());
    beastTestHelper = new BeastTestHelper();
  });

  beforeEach('clearDatabase', async () => {
    await givenEmptyDatabase();
  });

  after(async () => {
    await app.stop();
  });

  it('[get] returns all beast classes', async () => {
    const beastClass = await beastTestHelper.givenBeastClass();

    const response = await client.get('/beast-class');

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql(beastClass.toJSON());
  });

  it('[get] returns a single beast class by id', async () => {
    const beastClass = await beastTestHelper.givenBeastClass();

    const response = await client.get(`/beast-class/${beastClass.getId()}`);

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql({
      id: beastClass.getId().toString(),
      name: beastClass.name,
      description: beastClass.description,
      createdAt: beastClass.createdAt.toISOString(),
    });
  });

  it('[get] returns count of beast classes', async () => {
    await beastTestHelper.givenBeastClass();

    const response = await client.get('/beast-class/count');

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql({count: 1});
  });

  it('[post] creates a beast class', async () => {
    const body = {
      name: 'a-beast-class-name',
      description: 'a-beast-class-description',
    };

    const response = await client.post('/beast-class').send(body);

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    const createdBeastClass = await beastTestHelper.beastClassRepository.findById(
      response.body.id,
    );

    expect(createdBeastClass).to.be.instanceOf(BeastClass);
  });

  it('[post] validation error for beast class "name"', async () => {
    const body = {
      description: 'a-beast-class-description',
    };

    const response = await client.post('/beast-class').send(body);

    // assertions
    expect(response.status).to.equal(422);

    expect(response.headers).to.containEql({
      'content-type': 'application/json; charset=utf-8',
    });

    expect(response.body.error.code).to.containEql('VALIDATION_FAILED');

    expect(response.body.error.details[0]).to.containEql({
      message: "should have required property 'name'",
    });
  });

  it('[post] validation error for beast class "description"', async () => {
    const body = {
      name: 'a-beast-class-name',
    };

    const response = await client.post('/beast-class').send(body);

    // assertions
    expect(response.status).to.equal(422);

    expect(response.headers).to.containEql({
      'content-type': 'application/json; charset=utf-8',
    });

    expect(response.body.error.code).to.containEql('VALIDATION_FAILED');

    expect(response.body.error.details[0]).to.containEql({
      message: "should have required property 'description'",
    });
  });

  it('[delete] deletes a single beast class by id', async () => {
    const beastClass = await beastTestHelper.givenBeastClass();

    const deleteResponse = await client.delete(
      `/beast-class/${beastClass.getId()}`,
    );

    const beastClasses = await beastTestHelper.beastClassRepository.find();

    // assertions
    expect(deleteResponse.noContent).to.be.true();

    expect(beastClasses.length).to.equal(0);
  });

  it('[delete] validation error for beast class "id"', async () => {
    const deleteResponse1 = await client.delete('/beast-class/');

    const deleteResponse2 = await client.delete('/beast-class/abcd');

    // assertions
    expect(deleteResponse1.notFound).to.be.true();

    expect(deleteResponse1.body.error.name).to.equal('NotFoundError');

    expect(deleteResponse2.notFound).to.be.true();

    expect(deleteResponse2.body.error.code).to.equal('ENTITY_NOT_FOUND');
  });

  it('[patch] updates a single beast class by id', async () => {
    const patchBody = {
      name: 'new-name',
      description: 'new-description',
    };

    const beastClass = await beastTestHelper.givenBeastClass();

    const patchResponse = await client
      .patch(`/beast-class/${beastClass.getId()}`)
      .send(patchBody);

    const updatedBeastClass = await beastTestHelper.beastClassRepository.findById(
      beastClass.getId(),
    );

    // assertions
    expect(patchResponse.noContent).to.be.true();

    expect(updatedBeastClass.name).to.equal(patchBody.name);

    expect(updatedBeastClass.description).to.equal(patchBody.description);
  });
});
