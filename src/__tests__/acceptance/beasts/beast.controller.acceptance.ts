import {Client, expect} from '@loopback/testlab';
import {WitcherApiLb4Application} from '../../..';
import {Beast} from '../../../models';
import {setupApplication} from '../../helpers/app/app-helper';
import BeastTestHelper from '../../helpers/database/beast.test.helpers';
import {givenEmptyDatabase} from '../../helpers/database/database.helpers';

describe('BeastController (integration)', () => {
  let app: WitcherApiLb4Application;
  let client: Client;
  let beastTestHelper: BeastTestHelper;

  before('setupApplication', async () => {
    ({app, client} = await setupApplication());
    beastTestHelper = new BeastTestHelper();
  });

  beforeEach('clearDatabase', async () => {
    await givenEmptyDatabase();
  });

  after(async () => {
    await app.stop();
  });

  it('[get] returns all beasts', async () => {
    const beast = await beastTestHelper.givenBeast();

    const response = await client.get('/beasts');

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql(beast.toJSON());
  });

  it('[get] returns a single beast by id', async () => {
    const beast = await beastTestHelper.givenBeast();

    const response = await client.get(`/beasts/${beast.getId()}`);

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql({
      id: beast.getId().toString(),
      beastClassId: beast.beastClassId,
      createdAt: beast.createdAt.toISOString(),
    });
  });

  it('[get] returns count of beasts', async () => {
    await beastTestHelper.givenBeast();

    const response = await client.get('/beasts/count');

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    expect(response.body).to.containEql({count: 1});
  });

  it('[post] creates a beast', async () => {
    const beastClass = await beastTestHelper.givenBeastClass();

    const body = {
      name: 'a-beast-name',
      beastClassId: beastClass.getId(),
    };

    const response = await client.post('/beasts').send(body);

    // assertions
    expect(response.ok).to.be.true();

    expect(response.headers).to.containEql({
      'content-type': 'application/json',
    });

    const createdBeast = await beastTestHelper.beastRepository.findById(
      response.body.id,
    );

    expect(createdBeast).to.be.instanceOf(Beast);
  });

  it('[post] validation error for beast "beastClassId"', async () => {
    const body = {
      name: 'a-beast-name',
    };

    const response = await client.post('/beasts').send(body);

    // assertions
    expect(response.status).to.equal(422);

    expect(response.headers).to.containEql({
      'content-type': 'application/json; charset=utf-8',
    });

    expect(response.body.error.code).to.containEql('VALIDATION_FAILED');

    expect(response.body.error.details[0]).to.containEql({
      message: "should have required property 'beastClassId'",
    });
  });

  it('[post] validation error for beast "name"', async () => {
    const body = {
      beastClassId: 'some-beast-class-id',
    };

    const response = await client.post('/beasts').send(body);

    // assertions
    expect(response.status).to.equal(422);

    expect(response.headers).to.containEql({
      'content-type': 'application/json; charset=utf-8',
    });

    expect(response.body.error.code).to.containEql('VALIDATION_FAILED');

    expect(response.body.error.details[0]).to.containEql({
      message: "should have required property 'name'",
    });
  });

  it('[delete] deletes a single beast by id', async () => {
    const beast = await beastTestHelper.givenBeast();

    const deleteResponse = await client.delete(`/beasts/${beast.getId()}`);

    const beasts = await beastTestHelper.beastRepository.find();

    // assertions
    expect(deleteResponse.noContent).to.be.true();

    expect(beasts.length).to.equal(0);
  });

  it('[delete] validation error for beast "id"', async () => {
    const deleteResponse1 = await client.delete('/beasts/');

    const deleteResponse2 = await client.delete('/beasts/abcd');

    // assertions
    expect(deleteResponse1.notFound).to.be.true();

    expect(deleteResponse1.body.error.name).to.equal('NotFoundError');

    expect(deleteResponse2.notFound).to.be.true();

    expect(deleteResponse2.body.error.code).to.equal('ENTITY_NOT_FOUND');
  });

  it('[patch] updates a single beast by id', async () => {
    const newBeastClass = await beastTestHelper.givenBeastClass();

    const patchBody = {
      name: 'new-name',
      beastClassId: newBeastClass.getId(),
    };

    const beast = await beastTestHelper.givenBeast();

    const patchResponse = await client
      .patch(`/beasts/${beast.getId()}`)
      .send(patchBody);

    const updatedBeast = await beastTestHelper.beastRepository.findById(
      beast.getId(),
    );

    // assertions
    expect(patchResponse.noContent).to.be.true();

    expect(updatedBeast.name).to.equal(patchBody.name);

    // expect(updatedBeast.beastClassId).to.equal(newBeastClass.id);
  });
});
