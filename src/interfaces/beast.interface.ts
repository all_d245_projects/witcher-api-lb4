import {Collectable, Location} from '../models';
import {Magic} from './magic.interface';

export interface Beast {
  name: string;
  beastClassId: string;
  image?: string;
  variations?: Array<Beast>;
  occurrence?: Array<Location>;
  susceptibility?: Array<Collectable | Magic>;
  immunity?: Array<Collectable>;
  loot?: Array<Collectable>;
}
